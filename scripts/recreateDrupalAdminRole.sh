#!/bin/bash

# Whenever requested by user, this will generate a AdminRole in AuthzAPI

usage() { echo "Usage: $0 [--project <PROJECT>]" 1>&2; exit 1;  }
# Options
ARGS=$(getopt -o 'p:' --long 'project:' -- "$@") || exit 1
eval "set -- $ARGS"

while true; do
  case "$1" in
    (-p|--project)
      PROJECT="$2"; shift 2;;
    (--) shift; break;;
    (*) usage;;
  esac
done

[[ -z "${KUBECONFIG}"  ]] && echo "No cluster access!" && usage
[[ -z "${PROJECT}"  ]] && usage

ROLE_NAME="recreated-administrator"
ADMIN='
{
    "apiVersion": "webservices.cern.ch/v1alpha1",
    "kind": "BootstrapApplicationRole",
    "metadata": {
        "name": "'${ROLE_NAME}'",
        "namespace": "'${PROJECT}'"
     },
    "spec": {
         "applyToAllUsers": false,
         "description": "Role for Administrators of the Drupal website",
         "displayName": "Administrator",
         "minLevelOfAssurance": 5,
         "multifactorRequired": false,
         "name": "administrator",
         "required": false
    }
}
'
createRole=$(echo ${ADMIN} | oc apply -f -)
echo $createRole
