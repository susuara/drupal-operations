# Drupal operations

This project includes operations on Drupal sites or across infrastructure that are not part of the drupalSite-operator, such as Tekton tasks.

We implement actions that the infrastructure users can apply ad-hoc to their websites, and also other infrastructure components can use to perform their tasks.

## Examples

In Examples there are some one-off operations that show how something could be done, without providing full automation.
